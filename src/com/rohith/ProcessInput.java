package com.rohith;

import java.util.Scanner;

/**
 * @author Rohith
 *
 */
public class ProcessInput {

	@SuppressWarnings("resource")
	public static PatientHistory read() {
		PatientHistory patientHistory = new PatientHistory();
		CurrentHealth currentHealth = new CurrentHealth();
		Habits habits = new Habits();

		System.out.println("Enter Name:");
		Scanner addName = new Scanner(System.in);
		patientHistory.setName(addName.nextLine());

		System.out.println("Enter Age:");
		Scanner addAge = new Scanner(System.in);
		patientHistory.setAge(addAge.nextInt());

		System.out.println("Enter Gender (Male /Female /Others):");
		Scanner addGender = new Scanner(System.in);
		patientHistory.setGender(addGender.nextLine());

		System.out.println("Current Health");

		System.out.println("Hyper Tension (Yes/No)");
		Scanner addHyperTeanion = new Scanner(System.in);
		currentHealth.setHypertension(addHyperTeanion.nextLine());

		System.out.println("Blood Pressure (Yes/No)");
		Scanner addBloodPressure = new Scanner(System.in);
		currentHealth.setBloodPressure(addBloodPressure.nextLine());

		System.out.println("Blood Sugar (Yes/No)");
		Scanner addBloodSugar = new Scanner(System.in);
		currentHealth.setBloodSugar(addBloodSugar.nextLine());

		System.out.println("Over Weight (Yes/No)");
		Scanner addOverWeight = new Scanner(System.in);
		currentHealth.setOverWeight(addOverWeight.nextLine());

		patientHistory.setCurrentHealth(currentHealth);

		System.out.println("Enter Habits");
		System.out.println("Smoking (Yes/No)");
		Scanner addSmoke = new Scanner(System.in);
		habits.setSmoking(addSmoke.nextLine());

		System.out.println("Alchol(Yes/No)");
		Scanner addAlchol = new Scanner(System.in);
		habits.setAlchol(addAlchol.nextLine());

		System.out.println("Daily Excercise (Yes/No)");
		Scanner addExcercise = new Scanner(System.in);
		habits.setDailyExcercise(addExcercise.nextLine());

		System.out.println("Drugs (Yes/No)");
		Scanner addDrugs = new Scanner(System.in);
		habits.setDrugs(addDrugs.nextLine());

		patientHistory.setHabits(habits);

		return patientHistory;

	}

}

package com.rohith;

/**
 * @author Rohith
 *
 */
public class Habits {
	String smoking;
	String alchol;
	String dailyExcercise;
	String drugs;

	/**
	 * @return the smoking
	 */
	public String getSmoking() {
		return smoking;
	}

	/**
	 * @param smoking
	 *            the smoking to set
	 */
	public void setSmoking(String smoking) {
		this.smoking = smoking;
	}

	/**
	 * @return the alchol
	 */
	public String getAlchol() {
		return alchol;
	}

	/**
	 * @param alchol
	 *            the alchol to set
	 */
	public void setAlchol(String alchol) {
		this.alchol = alchol;
	}

	/**
	 * @return the dailyExcercise
	 */
	public String getDailyExcercise() {
		return dailyExcercise;
	}

	/**
	 * @param dailyExcercise
	 *            the dailyExcercise to set
	 */
	public void setDailyExcercise(String dailyExcercise) {
		this.dailyExcercise = dailyExcercise;
	}

	/**
	 * @return the drugs
	 */
	public String getDrugs() {
		return drugs;
	}

	/**
	 * @param drugs
	 *            the drugs to set
	 */
	public void setDrugs(String drugs) {
		this.drugs = drugs;
	}

}

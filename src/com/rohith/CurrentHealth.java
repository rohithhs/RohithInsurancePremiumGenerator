package com.rohith;

/**
 * @author Rohith
 *
 */
public class CurrentHealth {
	String hypertension;
	String bloodPressure;
	String bloodSugar;
	String overWeight;

	/**
	 * @return the hypertension
	 */
	public String getHypertension() {
		return hypertension;
	}

	/**
	 * @param hypertension
	 *            the hypertension to set
	 */
	public void setHypertension(String hypertension) {
		this.hypertension = hypertension;
	}

	/**
	 * @return the bloodPressure
	 */
	public String getBloodPressure() {
		return bloodPressure;
	}

	/**
	 * @param bloodPressure
	 *            the bloodPressure to set
	 */
	public void setBloodPressure(String bloodPressure) {
		this.bloodPressure = bloodPressure;
	}

	/**
	 * @return the bloodSugar
	 */
	public String getBloodSugar() {
		return bloodSugar;
	}

	/**
	 * @param bloodSugar
	 *            the bloodSugar to set
	 */
	public void setBloodSugar(String bloodSugar) {
		this.bloodSugar = bloodSugar;
	}

	/**
	 * @return the overWeight
	 */
	public String getOverWeight() {
		return overWeight;
	}

	/**
	 * @param overWeight
	 *            the overWeight to set
	 */
	public void setOverWeight(String overWeight) {
		this.overWeight = overWeight;
	}

}

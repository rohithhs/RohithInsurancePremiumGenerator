package com.rohith;

/**
 * @author Rohith
 *
 */
public class PatientHistory {
	String name;
	String gender;
	int age;

	CurrentHealth currentHealth;
	Habits habits;

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param name
	 *            the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	/**
	 * @return the gender
	 */
	public String getGender() {
		return gender;
	}

	/**
	 * @param gender
	 *            the gender to set
	 */
	public void setGender(String gender) {
		this.gender = gender;
	}

	/**
	 * @return the age
	 */
	public int getAge() {
		return age;
	}

	/**
	 * @param age
	 *            the age to set
	 */
	public void setAge(int age) {
		this.age = age;
	}

	/**
	 * @return the currentHealth
	 */
	public CurrentHealth getCurrentHealth() {
		return currentHealth;
	}

	/**
	 * @param currentHealth
	 *            the currentHealth to set
	 */
	public void setCurrentHealth(CurrentHealth currentHealth) {
		this.currentHealth = currentHealth;
	}

	/**
	 * @return the habits
	 */
	public Habits getHabits() {
		return habits;
	}

	/**
	 * @param habits
	 *            the habits to set
	 */
	public void setHabits(Habits habits) {
		this.habits = habits;
	}

}

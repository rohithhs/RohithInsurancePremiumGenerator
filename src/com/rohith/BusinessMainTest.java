/**
 * 
 */
package com.rohith;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

/**
 * @author Rohith
 *
 */
public class BusinessMainTest {

	@Test
	public void computeValidPremiumTest() {
		// For Valid Data
		PatientHistory patientValidHistory = mockValidPatientHistory();

		double validPremium = BusinessMain.computePremium(patientValidHistory);

		double mockValidPremiumAmount = mockValidPremiumAmount();
		assertEquals(mockValidPremiumAmount, validPremium, 0.0);

	}

	@Test
	public void computeInValidPremiumTest() {

		// For InValid Data
		PatientHistory patientInValidHistory = mockInValidPatientHistory();

		double inValidPremium = BusinessMain.computePremium(patientInValidHistory);

		double mockInValidPremiumAmount = mockInValidPremiumAmount();
		assertEquals(mockInValidPremiumAmount, inValidPremium, 0.0);

	}

	@Test
	public void computeNullPremiumTest() {

		// For Null Data
		PatientHistory patientNullHistory = mockNullPatientHistory();

		double nullPremium = BusinessMain.computePremium(patientNullHistory);

		double mockNullPremiumAmount = mockNullPremiumAmount();
		assertEquals(mockNullPremiumAmount, nullPremium, 0.0);

	}

	private double mockValidPremiumAmount() {
		double premiumAmount = 6690.27333;
		return premiumAmount;
	}

	private double mockInValidPremiumAmount() {
		double premiumAmount = 5000.00;
		return premiumAmount;
	}

	private double mockNullPremiumAmount() {
		double premiumAmount = 0.0;
		return premiumAmount;
	}

	private PatientHistory mockValidPatientHistory() {
		PatientHistory patientValidHistory = new PatientHistory();
		patientValidHistory.setName("Rohith");
		patientValidHistory.setAge(34);
		patientValidHistory.setGender("Male");

		CurrentHealth currentHealth = new CurrentHealth();
		currentHealth.setBloodPressure("No");
		currentHealth.setBloodSugar("No");
		currentHealth.setHypertension("No");
		currentHealth.setOverWeight("Yes");

		patientValidHistory.setCurrentHealth(currentHealth);

		Habits habits = new Habits();
		habits.setAlchol("Yes");
		habits.setSmoking("No");
		habits.setDailyExcercise("Yes");
		habits.setDrugs("No");

		patientValidHistory.setHabits(habits);

		return patientValidHistory;

	}

	private PatientHistory mockInValidPatientHistory() {
		PatientHistory patientInValidHistory = new PatientHistory();
		patientInValidHistory.setName("Roh32ith");
		patientInValidHistory.setAge(04);
		patientInValidHistory.setGender("Male!@");

		CurrentHealth currentHealth = new CurrentHealth();
		currentHealth.setBloodPressure("!@WA");
		currentHealth.setBloodSugar("$%");
		currentHealth.setHypertension("scdds");
		currentHealth.setOverWeight("vrvwv");

		patientInValidHistory.setCurrentHealth(currentHealth);

		Habits habits = new Habits();
		habits.setAlchol("122132");
		habits.setSmoking("AQDw");
		habits.setDailyExcercise("@#$%");
		habits.setDrugs("yuyu");

		patientInValidHistory.setHabits(habits);

		return patientInValidHistory;

	}

	private PatientHistory mockNullPatientHistory() {
		PatientHistory patientNullHistory = new PatientHistory();
		patientNullHistory.setName(null);
		patientNullHistory.setAge(0);
		patientNullHistory.setGender(null);

		CurrentHealth currentHealth = new CurrentHealth();
		currentHealth.setBloodPressure(null);
		currentHealth.setBloodSugar(null);
		currentHealth.setHypertension(null);
		currentHealth.setOverWeight(null);

		patientNullHistory.setCurrentHealth(currentHealth);

		Habits habits = new Habits();
		habits.setAlchol(null);
		habits.setSmoking(null);
		habits.setDailyExcercise(null);
		habits.setDrugs(null);

		patientNullHistory.setHabits(habits);

		return patientNullHistory;

	}

}

package com.rohith;

/**
 * @author Rohith
 *
 */
public class InsuranceQuoteGenerator {

	public static void main(String[] args) {

		PatientHistory history = ProcessInput.read();
		double computedPremium = BusinessMain.computePremium(history);
		System.out.println("Computed premium amount:" + computedPremium);

	}

}

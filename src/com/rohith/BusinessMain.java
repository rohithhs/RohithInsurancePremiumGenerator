package com.rohith;

/**
 * @author Rohith
 *
 */
public class BusinessMain {

	public static double computePremium(PatientHistory history) {

		double basePremium = 0.0;
		if (history != null && history.age > 0) {
			basePremium = 5000.00;

			if (history.age > 18 && history.age <= 25) {
				double premiumIncrement = basePremium * 10 / 100;
				basePremium += premiumIncrement;
			}
			if (history.age > 25 && history.age <= 30) {
				double premiumIncrement = basePremium * 20 / 100;
				basePremium += premiumIncrement;
			}
			if (history.age > 30 && history.age <= 35) {
				double premiumIncrement = basePremium * 30 / 100;
				basePremium += premiumIncrement;
			}
			if (history.age > 35 && history.age <= 40) {
				double premiumIncrement = basePremium * 40 / 100;
				basePremium += premiumIncrement;
			}
			if (history.age > 40) {
				double premiumIncrement = basePremium * 60 / 100;
				basePremium += premiumIncrement;
			}

			if (history.gender != null) {
				if (history.gender.equalsIgnoreCase("Male")) {
					double premiumIncrement = basePremium * 2 / 100;
					basePremium += premiumIncrement;
				}
			}

			if (history.currentHealth != null && history.currentHealth.hypertension != null
					&& history.currentHealth.bloodPressure != null && history.currentHealth.bloodSugar != null
					&& history.currentHealth.overWeight != null) {
				if (history.currentHealth.hypertension.equalsIgnoreCase("Yes")
						|| history.currentHealth.bloodPressure.equalsIgnoreCase("Yes")
						|| history.currentHealth.bloodSugar.equalsIgnoreCase("Yes")
						|| history.currentHealth.overWeight.equalsIgnoreCase("Yes")) {
					double premiumIncrement = basePremium * 1 / 100;
					basePremium += premiumIncrement;
				}
			}

			if (history.habits != null && history.habits.dailyExcercise != null) {

				if (history.habits.dailyExcercise.equalsIgnoreCase("Yes")) {
					double premiumIncrement = basePremium * 3 / 100;
					basePremium -= premiumIncrement;
				}
			}

			if (history.habits != null && history.habits.smoking != null && history.habits.drugs != null
					&& history.habits.alchol != null) {
				if (history.habits.smoking.equalsIgnoreCase("Yes") || history.habits.drugs.equalsIgnoreCase("Yes")
						|| history.habits.alchol.equalsIgnoreCase("Yes")) {
					double premiumIncrement = basePremium * 3 / 100;
					basePremium += premiumIncrement;
				}
			}
		}

		return basePremium;
	}
}
